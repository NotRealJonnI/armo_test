﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ARMO_Test.Properties;

namespace ARMO_Test
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            //Загрузка сохраненных критериев
            textBox1.Text = Settings.Default["Path"].ToString();
            textBox2.Text = Settings.Default["TemplateFileName"].ToString();
            textBox3.Text = Settings.Default["TextExample"].ToString();
        }
        Thread thread;
        //Остановка поиска
        private volatile bool _pause;
        private static ManualResetEvent mre = new ManualResetEvent(true);

        private void Button1_Click(object sender, EventArgs e)
        {
            //Запуск поиска
            if (thread != null)
            {
                thread.Abort();
                _pause = false;
                mre.Set();
                button3.Text = "Стоп";
            }
            thread = new Thread(StartSearch);
            thread.Start();
            button1.Visible = false;
            button3.Visible = true;
        }

        private string path;
        private string TemplateFileName;
        private string TextExample;
        private void StartSearch()
        {
            path = Settings.Default["Path"].ToString();
            TemplateFileName = Settings.Default["TemplateFileName"].ToString();
            TextExample = Settings.Default["TextExample"].ToString();            
            if (Directory.Exists(path))
            {        
                this.Invoke((Action)(() =>
                {
                    treeView1.Nodes.Clear();
                    treeView1.Nodes.Add(path);
                    //Первое звено
                }));
                //Рекурсивный обход
                FillTreeNode(treeView1.Nodes[0], path);
            }
            else
            {
                MessageBox.Show("Данной директории не существует");
            }
        }
        private void FillTreeNode(TreeNode node, string path)
        {
            mre.WaitOne();
            //Рекурсивный обход папок
            string[] dirs = Directory.GetDirectories(path);
            foreach (string dir in dirs)
            {
                mre.WaitOne();
                TreeNode dirNode = new TreeNode();
                dirNode.Text = dir.Remove(0, dir.LastIndexOf("\\") + 1);
                AddNode(node, dirNode);
                //Thread.Sleep(1000);
                FillTreeNode(dirNode, path + "\\" + dirNode.Text);
            }
            //Проверка файлов
            string[] files = Directory.GetFiles(path);
            foreach (string file in files)
            {
                mre.WaitOne();
                //Thread.Sleep(1000);
                this.Invoke((Action)(() =>
                {
                    label4.Text = "Обработка " + file;
                }));
                //Если подразумевалось,что пользователь вводит регулярное выражение нужно было использовать Regex.IsMatch(file,TemplateFileName,RegexOptions.IgnoreCase);
                if (Path.GetFileNameWithoutExtension(file).Contains(TemplateFileName)//Сделал, что просто просто содержит слово или предлождение 
                    && Path.GetExtension(file).Contains("txt")//и читает только txt файлы 
                    )
                {
                    if (readFile(file, TextExample))
                    {
                        AddNode(node, file.Remove(0, file.LastIndexOf("\\") + 1));
                    }
                }
            }
        }

        //Добавление узла 
        private void AddNode(TreeNode node, string name)
        {
            this.Invoke((Action)(() =>
            {
                node.Nodes.Add(name);
            }));
        }
        private void AddNode(TreeNode node, TreeNode child)
        {
            this.Invoke((Action)(() =>
            {
                node.Nodes.Add(child);
            }));
        }
        //Поиск текста, содержащего в файле
        private bool readFile(string path, string text)
        {
            string[] data = File.ReadAllLines(path, Encoding.GetEncoding(1251));

            try
            {
                foreach (var line in data)
                {
                    if (!String.IsNullOrEmpty(line))
                    {
                        if (line.Contains(text))
                        {
                            return true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return false;
        }
        //Открытие диалогового окна для ввода директории
        private void Button2_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog Dir = new FolderBrowserDialog();
            if (Dir.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = Dir.SelectedPath;
            }
        }
        private void TextBox1_TextChanged(object sender, EventArgs e)
        {
            Settings.Default["Path"] = textBox1.Text;
            Settings.Default.Save();
        }
        private void TextBox2_TextChanged(object sender, EventArgs e)
        {
            Settings.Default["TemplateFileName"] = textBox2.Text;
            Settings.Default.Save();
        }

        private void TextBox3_TextChanged(object sender, EventArgs e)
        {
            Settings.Default["TextExample"] = textBox3.Text;
            Settings.Default.Save();
        }

        //Остановка поиска
        private void Button3_Click(object sender, EventArgs e)
        {

            button1.Visible = !button1.Visible;
            _pause = !_pause;

            if (_pause)
            {
                button3.Text = "Продолжить";
                mre.Reset();

            }
            else
            {
                button3.Text = "Стоп";
                mre.Set();
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(thread!=null) thread.Abort();
        }

    }
}
